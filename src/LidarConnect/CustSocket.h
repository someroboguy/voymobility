#pragma once
#include <cstdlib>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <chrono>
#include <thread>

//Todo: add cleanup and reconnect
//Todo: detect and stress test dropped connection detection

/**Basic boost socket implementation for client and server side communications
*Connect can only be called once per initialization of this class
*This class does not explicitly detect dropped connection *atm*
*Arbitrary max character length for messages of 1024 chars
*/
class BoostSoc
{
public:
	bool debugPrints = false;

	/**Default constructor
	* #_debugPrints: set this to true to enable print outs from try catch statements
	*/
	BoostSoc(bool _debugPrints = false) :connectedSocket(io_service)
	{
		debugPrints = debugPrints;
	}

	/**Creates a server on the port specified and begins listening.
	* If connection is found, socket will be created and server discarded
	* #port: is an int specifying the serial port to bind to
	* #blocking: determines whether the function will return immediately or after connection established
	*/
	void serverConnect(int port, bool blocking);

	/*Attempts to create a client socket connecting to a server at the ip and port specified
	* #port: int specifying the serial port of the server
	* #ip: string formatted ip address of server example: "127.0.0.1"
	* #blocking: determines whether the function will return immediately or after connection established
	* #connectionAttempts: (default 1) specifies how many times connect should be attempted (-1 for infinite)
	*/
	void clientConnect(int port, std::string ip, bool blocking, int connectionAttempts = 1);

	/**Reads all available bytes on connected socket
	* will return empty string on any failure
	* #return: recieved bytes typecasted directly to std::string
	*/
	std::string getMessage();

	/**Sends message as byte array on connected socket
	* Does nothing if socket not connected or fault caught
	*/
	void sendMessage(std::string message);

	bool getConnected() { return connected; }
	bool getListening() { return listening; }

	std::string getStatus()
	{
		if (listening) { return "listening"; }
		else if (connected) { return "connected"; }
		else { return "broken"; }
	}

private:
	boost::asio::io_service io_service;
	boost::asio::ip::tcp::socket connectedSocket;
	bool connected = false;
	bool listening = false;

	void serverConnectDoWork(int port);
	void clientConnectDoWork(boost::asio::ip::tcp::resolver::iterator iterator, int attempts);
	static const int max_Size = 1024;
};