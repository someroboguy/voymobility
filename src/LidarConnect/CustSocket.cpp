//#include "CustSocket.h"
//
//const boost::system::error_code ec;
//io_service ioservice;
//
//void handler(const boost::system::error_code &ec)
//{
//
//}
//
//
////This is if the user specifies that they are the client
//void sock::client(unsigned short port, std::string IP) {
//	//Creates a socket with a connection to the server
//	tcp::socket tcp_socket{ ioservice };
//	tcp::endpoint endpoint(address::from_string(IP), port);
//	tcp_socket.async_connect(endpoint, handler);
//
//	//Runs this if there is a connection
//	if (!ec) {
//		std::array<char, 4096> bytes;
//		std::string data;
//		std::cout << "Connected!" << std::endl;
//		while (!ec) {
//			//Checks the socket to see if it's able to pick up any data
//			int bytesAvailable = tcp_socket.available();
//			//If there's data to be receive, it is received and displayed for user
//			if (bytesAvailable > 0) {
//				int bytesRec = tcp_socket.receive(buffer(bytes));
//				std::cout << "Mobility Base: ";
//				std::cout.write(bytes.data(), bytesRec);
//				std::cout << std::endl;
//			}
//			//Allows user to enter a message and sends it if it doesn't say "bye" 
//			std::cout << "Enter msg: " << std::endl;
//			std::getline(std::cin, data);
//			if (data != "bye" && data != "hi" ) {
//				double x, y, theta, speed;
//				char comma;
//				tcp_socket.send(buffer(data));
//				std::istringstream split_data(data);
//				split_data >> x;
//				split_data >> comma;
//				split_data >> y;
//				split_data >> comma;
//				split_data >> theta;
//				split_data >> comma;
//				split_data >> speed;
//				std::cout << "Command Sent" << std::endl;
//				std::cout << std::endl;
//			}
//			else if (data == "hi") {
//				tcp_socket.send(buffer(data));
//			}
//			else {
//				tcp_socket.send(buffer(data));
//				std::cout << "Connection has ended" << std::endl;
//				break;
//			}
//		}
//		tcp_socket.shutdown(tcp::socket::shutdown_send);
//	}
//}


//Commented out code above is for reference for structure

#include "CustSocket.h"

void BoostSoc::serverConnect(int port, bool blocking)
{
	if (listening || connected) { return; }

	if (!blocking)
	{
		boost::thread backCon(boost::bind(&BoostSoc::serverConnectDoWork, this, port));
	}
	else
	{
		this->serverConnectDoWork(port);
	}
}

void BoostSoc::clientConnect(int port, std::string ip, bool blocking, int connectionAttempts)
{
	if (listening || connected) { return; }

	using boost::asio::ip::tcp;
	tcp::resolver resolver(io_service);
	tcp::resolver::query query(tcp::v4(), ip, std::to_string(port));
	tcp::resolver::iterator iterator = resolver.resolve(query);

	if (!blocking)
	{
		boost::thread backCon(boost::bind(&BoostSoc::clientConnectDoWork, this, iterator, connectionAttempts));
	}
	else
	{
		this->clientConnectDoWork(iterator, connectionAttempts);
	}
}

std::string BoostSoc::getMessage()
{
	if (!connected) { return ""; }

	try
	{
		std::size_t count = connectedSocket.available();
		if (count > 0)
		{
			char inMessage[max_Size];
			boost::asio::read(connectedSocket, boost::asio::buffer(inMessage, count));
			std::string trimmedMessage = std::string(inMessage, count);
			return trimmedMessage;
		}
		else
		{
			return "";
		}
	}
	catch (std::exception& e)
	{
		if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
		return "";
	}
}

void BoostSoc::sendMessage(std::string message)
{
	if (!connected) { return; }

	try
	{
		boost::asio::write(connectedSocket, boost::asio::buffer(message.c_str(), message.length()));
	}
	catch (std::exception& e)
	{
		if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
	}
}

void BoostSoc::serverConnectDoWork(int port)
{
	listening = true;
	try
	{
		using boost::asio::ip::tcp;
		tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
		a.accept(connectedSocket);
		connected = true;
	}
	catch (std::exception& e)
	{
		if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
		connected = false;
	}
	listening = false;
}

void BoostSoc::clientConnectDoWork(boost::asio::ip::tcp::resolver::iterator iterator, int attempts)
{
	listening = true;
	while (!connected && (attempts == -1 || attempts > 0))
	{
		try
		{
			boost::asio::connect(connectedSocket, iterator);
			connected = true;
		}
		catch (std::exception& e)
		{
			if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
			connected = false;
		}
		if (attempts != -1) { attempts -= 1; }
	}
	listening = false;
}