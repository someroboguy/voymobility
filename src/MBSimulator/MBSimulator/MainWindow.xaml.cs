﻿using System;
using System.Net;
using System.Net.Sockets; 
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace MBSimulator
{
    public partial class MainWindow : Window
    {
        Socket homeSocket = null; 
        Point3D playerPt = new Point3D(0, 0, 0);
        DispatcherTimer serialTimer = new DispatcherTimer();
        DispatcherTimer inputTimer = new DispatcherTimer();
        double speed = 0;
        double angle = 0;
        double xCmd = 0;
        double yCmd = 0;
       
        public MainWindow()
        {
            InitializeComponent();

            //Timers tick every half a second to see if a command has been pressed
            serialTimer.Tick += SerialTimer_Tick;
            serialTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            serialTimer.Start();
            
            inputTimer.Tick += InputTimer_Tick;
            inputTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            inputTimer.Start();
        }

        //Pressing the butoon will establish socket between Laptop and MB as well as R2000 Lidar
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            homeSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress homeAddress = IPAddress.Parse("127.0.0.1");
            //Insert IP address and Port
            IPEndPoint homeRemoteEP = new IPEndPoint(homeAddress, 2002);
            ((Socket)homeSocket).Connect(homeRemoteEP);
            
        }

        private void SerialTimer_Tick(object sender, EventArgs e)
        {
            if (homeSocket != null && homeSocket.Connected)
            {
                tbHomeStatus.Text = "Connected!";
            }
            else
            {
                tbHomeStatus.Text = "Listening...";
            }

        }

        //Sends the command when you press Enter button
        private void InputTimer_Tick(object sender, EventArgs e)
        {
            //Parsing will allow you to take the values from the text on the GUI and use its values
            //Send serially the desired position, speed, and angle                 
            if (homeSocket != null && homeSocket.Connected)
            {
                double.TryParse(tbSpeed.Text, out speed);
                double.TryParse(tbAngle.Text, out angle);
                double.TryParse(tbMoveX.Text, out xCmd);
                double.TryParse(tbMoveY.Text, out yCmd);
                if (Keyboard.IsKeyDown(Key.Enter))
                {
                    string moveCmd = xCmd.ToString();
                    moveCmd += ",";
                    moveCmd += yCmd.ToString();
                    moveCmd += ",";
                    moveCmd += angle.ToString();
                    moveCmd += ",";
                    moveCmd += speed.ToString();
                    moveCmd += ";";
                    homeSocket.Send(Encoding.ASCII.GetBytes(moveCmd));


            //Also receives the positioning of the Mobility Base from the visualizer after the transformation has been applied
                    byte[] bytesReceived = new byte[homeSocket.ReceiveBufferSize];
                    int bytesRead = homeSocket.Receive(bytesReceived);
                    string message = Encoding.ASCII.GetString(bytesReceived, 0, bytesRead);

                    if (message != "")
                    {
                        string position = message.Split(';')[0];
                        tbPositionX.Text = position.Split(',')[0];
                        tbPositionY.Text = position.Split(',')[1];
                    }
                }

            }
        }
    }
}


