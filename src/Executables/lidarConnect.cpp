#include "CustSocket.h"

/*Sets up connection to Mobility Base
Windows Laptop will be the client */

//This is a background thread that is constantly reading and displaying messages from the MB
void readIncomingMessages(BoostSoc* connectedSoc) {
	while (true)
	{
		std::string inMessage = connectedSoc->getMessage();
		if (inMessage != "")
		{
			std::cout << "\r";
			std::cout << "MB: " + inMessage << std::endl;
			std::cout << "Me: ";
		}
	}
}



int main()
{
	BoostSoc client;
	std::cout << "Connecting to Mobility Base..." << std::endl;
	unsigned short port = 27036;
	std::string IP = "192.168.220.115";
	client.clientConnect(port, IP, false, -1);

	//Sets up background thread for reading messages
	boost::thread readThread(boost::bind(readIncomingMessages, &client));


	while (true)
	{
		std::string outMessage;
		std::cout << "Me: ";
		std::getline(std::cin, outMessage);
		if (outMessage != "bye")
			client.sendMessage(outMessage);
		else
			break;
	}
	system("pause");
}