#include "customLidar.h"


int main()
{
	std::string lidar_IP = "192.168.0.16";
	int frequency = 100;		  //Max scan frequency is 100 HZ
	int scan_sample_size = 3600;  // Set samples per scan in the range [72,25200] (valid values are listed in manual)
								  // Try staying below 4000 as it won't pick up all the points

	lidar::connectLidar(lidar_IP, frequency, scan_sample_size);
}