cmake_minimum_required(VERSION 3.10)
#cmake_minimum_required(VERSION 2.8.3)
project(pepperl_fuchs_r2000)
set(CMAKE_BUILD_TYPE Debug)

#set(CMAKE_BUILD_TYPE Release)
#set(CMAKE_BUILD_TYPE RelWithDebInfo)
#set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++0x -Wfatal-errors")

find_package(PCL 1.8 REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_subdirectory(Executables)
add_subdirectory(LidarCommunication)
add_subdirectory(LidarConnect)
