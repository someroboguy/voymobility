#include "customLidar.h"

std::vector<std::string> split(const char *str, char c);
void rotateAny(Eigen::Matrix4f &inFrame, Eigen::RowVector3f pt, Eigen::RowVector3f dir, double angle);
void translate(Eigen::Matrix4f &inFrame, double inputX, double inputY);
double *comparePoints(Eigen::Matrix4f &checkFrame, Eigen::Matrix4f &currentFrame);
void receiveCommand(BoostSoc* guiSocket, BoostSoc* baseClient, boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, std::string centerShape);

const long long charCount = 3600;
double *xPt;
double *yPt;

Eigen::Matrix4f ghostFrame = Eigen::Matrix4f::Identity();
Eigen::Matrix4f roboFrame = Eigen::Matrix4f::Identity();
Eigen::Matrix4f visFrame = Eigen::Matrix4f::Identity();

//Allows for the connection of R2000 Lidar as well as displaying the R2000 settings
void lidar::connectLidar(std::string lidarIP, int frequency, int samples_per_scan) 
{
	pepperl_fuchs::R2000Driver driver;
	bool success;

	//Connects to the lidar and sets the scan frequency as well as the samples per scan
	success = driver.connect(lidarIP);  
	success = driver.setScanFrequency(frequency);     										 
	success = driver.setSamplesPerScan(samples_per_scan);  

	//NOTE:  Can uncomment if you want to see all the parameters of the R2000 Lidar
	////Gets all the parameter and displays them for the user										  
	//auto params = driver.getParameters();
	//std::cout << "Displaying R2000 Scanner Settings:" << std::endl;
	//std::cout << "______________________________________________" << std::endl;
	//for (auto key_value : params)
	//{
	//	std::cout << key_value.first << ": " << key_value.second << std::endl;
	//}
	//std::cout << "_______________________________________________" << std::endl;

	//Captures data via TCP and sends it to captureData function
	success = driver.startCapturingTCP();      // Note: startCapturingUDP() also exists
	lidar::captureData(driver, success, samples_per_scan);
}

//Captures the data coming from the R2000 Lidar, converts it to x,y coordinates and displays it in a PCL Visualizer
void lidar::captureData(pepperl_fuchs::R2000Driver driver, bool success, int samples_per_scan)
{
	if (success) 
	{
		xPt = (double *)malloc(charCount * sizeof(double));
		yPt = (double *)malloc(charCount * sizeof(double));
		std::cout << "Congratulations! You are connected to R2000 Lidar HD!" << std::endl;

		//False is to silence all the failed connection stuff
		BoostSoc guiSocket(false);
		//False is for no blocking and -1 is for infinite attempts
		guiSocket.serverConnect(2002, false);

		//Sets up socket with the Mobility Base
		BoostSoc baseSocket(false);
		baseSocket.clientConnect(27036, "192.168.220.115", false);

		//Sets up PCL Visualizer for the Lidar
		boost::shared_ptr<pcl::visualization::PCLVisualizer> lidar_viewer(new pcl::visualization::PCLVisualizer("Lidar 2D Viewer"));
		lidar_viewer->setBackgroundColor(0, 0, 0);          
		lidar_viewer->addCoordinateSystem(50);
		lidar_viewer->initCameraParameters();
		std::string cube = "cube";
		float r = 1;
		float g = 0.549;
		float b = 0;

		//Sets up the cube which represents the base
		lidar_viewer->addCube(-200, 200, -200, 200, 0, 200, r, g, b, cube);
		lidar_viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, cube);
		//This sets up the camera parameters to allow you to see the cube in the right position 
		lidar_viewer->setCameraPosition(-148.831, -187.568, 3480, 0.000880995, 0.998547, 0.0538736, 0);


		//While connected to the R2000 Lidar display the data
		while (!lidar_viewer->wasStopped())
		{
			boost::thread serverThread(boost::bind(receiveCommand, &guiSocket, &baseSocket, lidar_viewer, cube));
			lidar_viewer->removeAllPointClouds();

			//Collects the data from the scanner
			pepperl_fuchs::ScanData  scandata = driver.getFullScan();
			scandata.headers;
			scandata.distance_data;							//Gives the distance object is from the lidar in polar form in mm
			scandata.amplitude_data;						//Amplitude data is how close the point is to change color (Don't worry about it for now)

			//Sets up the point cloud that will contain the points coming from the Lidar 
			pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
			pcl::PointXYZRGB data_point;
			float scale = 0.001;

			//Calculates the angle increment based on the number of samples per scan set by user in lidarRun.cpp
			double angle_increment = 360 * (1.0 / samples_per_scan);
			double theta = 0;

			//For every point that is scanned from the lidar (Distance is in polar form in mm)
			//3600 is used to represent the samples per scan
			for (int i = 0; i < 3600; i++)
			{
				if (theta < 360) 
				{
					//theta is in degrees so it's converted into radian to be calculated
					double radian = theta * (M_PI / 180);

					//Calculates the unit coordinates based off of the angle
					//Negative yu to be able to view it in the correct orientation
					double xu = sin(radian);
					double yu = -cos(radian);

					/*Calculates and sets the (x,y,z) coordinates from distance collected
					Note:  Points are given in mm 
					Note:  z coordinate is 0 because R2000 Lidar is in 2D*/
					double x = scandata.distance_data[i] * xu;
					double y = scandata.distance_data[i] * yu;
					data_point.x = x;
					data_point.y = y;
					data_point.z = 0;

					data_point.r = 255;
					data_point.g = 255;
					data_point.b = 255;

					cloud->push_back(data_point);

					/*Increments angle by 0.1 degrees as the samples collected per spin is 3600
					[360 degrees/3600 points = 0.1 degree increment per point]*/
					theta = theta + angle_increment;

					//Stores the X and Y point into an array for comparison later
					xPt[i] = scale * x;
					yPt[i] = scale * y;
				}
				else
				{
					theta = 0;
				}
			}
			//Adds Point Cloud with all the data points into the viewer and displays it for user
			lidar_viewer->addPointCloud<pcl::PointXYZRGB>(cloud, "lidar cloud");
			lidar_viewer->spinOnce(10);
		}
		driver.stopCapturing();
		driver.disconnect();
	}
}

//Splits up the string to a specific character
std::vector<std::string> split(const char *str, char c)
{
	std::vector<std::string> result;
	do
	{
		const char *begin = str;
		while (*str != c && *str)
			str++;
		result.push_back(std::string(begin, str));
	} while (0 != *str++);

	return result;
}

//Rotational math about a specific point
void rotateAny(Eigen::Matrix4f &inFrame, Eigen::RowVector3f pt, Eigen::RowVector3f dir, double angle)
{
	//https://sites.google.com/site/glennmurray/Home/rotation-matrices-and-formulas/rotation-about-an-arbitrary-axis-in-3-dimensions
	Eigen::Matrix4f transformation = Eigen::Matrix4f::Identity();

	dir.normalize();

	//pt: a,b,c
	//dir: u,v,w
	float a = pt(0);
	float b = pt(1);
	float c = pt(2);

	float u = dir(0);
	float v = dir(1);
	float w = dir(2);

	transformation(0, 0) = u * u + (v * v + w * w) * cos(angle);
	transformation(0, 1) = u * v * (1.0 - cos(angle)) - w * sin(angle);
	transformation(0, 2) = u * w * (1.0 - cos(angle)) + v * sin(angle);
	transformation(0, 3) = (a * (v * v + w * w) - u * (b * v + c * w)) * (1.0 - cos(angle)) + (b * w - c * v) * sin(angle);

	transformation(1, 0) = u * v * (1.0 - cos(angle)) + w * sin(angle);
	transformation(1, 1) = v * v + (u * u + w * w) * cos(angle);
	transformation(1, 2) = v * w * (1.0 - cos(angle)) - u * sin(angle);
	transformation(1, 3) = (b * (u * u + w * w) - v * (a * u + c * w)) * (1.0 - cos(angle)) + (c * u - a * w) * sin(angle);

	transformation(2, 0) = u * w * (1.0 - cos(angle)) - v * sin(angle);
	transformation(2, 1) = v * w * (1.0 - cos(angle)) + u * sin(angle);
	transformation(2, 2) = w * w + (u * u + v * v) * cos(angle);
	transformation(2, 3) = (c * (u * u + v * v) - w * (a * u + b * v)) * (1.0 - cos(angle)) + (a * v - b * u) * sin(angle);

	transformation(3, 0) = 0;
	transformation(3, 1) = 0;
	transformation(3, 2) = 0;
	transformation(3, 3) = 1.0;

	inFrame = transformation * inFrame;
}

//Applies the translation with respect to the new rotated position
void translate(Eigen::Matrix4f &inFrame, double inputX, double inputY)
{
	inFrame(0, 3) += inputX * inFrame(0, 0);
	inFrame(1, 3) += inputX * inFrame(1, 0);

	inFrame(0, 3) += inputY * inFrame(0, 1);
	inFrame(1, 3) += inputY * inFrame(1, 1);
}

/*Compares the points sent in from the ghost frame and current frame to the data from the R2000 Lidar.  
* If there are any collision points, then use that y-coordinate to find the x-coordinate to move to.
* If there aren't any collisions, then return the position sent in by the user. */
double *comparePoints(Eigen::Matrix4f &checkFrame, Eigen::Matrix4f &currentFrame)
{
	//Base radius is used to set a circle around the Mobility Base to avoid collisions
	double baseRadius = 0.55;

	double currentX = currentFrame(0, 3);
	double currentY = currentFrame(1, 3);
	double oldX = currentX;
	double oldY = currentY;
	double newX = checkFrame(0, 3);
	double newY = checkFrame(1, 3);

	//Divides the temp into 1000 check points
	double tempX = newX * 0.001;
	double tempY = newY * 0.001;
	
	double *moveToPt = new double[100];
	int collisionCount = 0;

	//Creates an invisible line with 1000 points and checks all 3600 points from lidar to see if there are any collision with those points
	for (int i = 0; i < 1000; i++)
	{
		collisionCount = 0;
		//Checks to see if there are any points from the lidar that are between the base and indexed point
		for (int index = 0; index < charCount; index++)
		{
			double compare = pow(xPt[index] - oldX, 2) + pow(yPt[index] - oldY, 2);
			double currentPtCompare = pow(xPt[index] - currentX, 2) + pow(yPt[index] - currentY, 2);

			if ((compare < pow(baseRadius, 2)) && (currentPtCompare < pow(baseRadius, 2)))
			{
				//Ignore the point because the data is from the base itself
			}
			//The collisionCount is used to be able to make sure that there are multiple points there, not just one random point
			else if ((compare > pow(baseRadius, 2)) && (currentPtCompare < pow(baseRadius,2)))
			{
				collisionCount++;
			}

			//If i == 0, then it is at the first point, so no modification is needed to be made
			if ((i == 0) && (collisionCount == 5))
			{
				moveToPt[1] = currentY;
				goto checkXPoints;
			}
			//This goes back to the index before the tested index to move the user to that position before hitting the obstacle
			else if ((collisionCount == 5) && (i != 0))
			{
				currentY = currentY - tempY;
				moveToPt[1] = currentY;
				goto checkXPoints;
			}
		}
		currentY = currentY + tempY;
	}

	//Returns the new position that was sent by the user since it was successful in testing
	moveToPt[1] = newY;
	goto checkXPoints;


checkXPoints:
	for (int i = 0; i < 1000; i++)
	{
		collisionCount = 0;
		//Checks to see if there are any points from the lidar that are between the base and indexed point
		for (int index = 0; index < charCount; index++)
		{
			double compare = pow(xPt[index] - oldX, 2) + pow(yPt[index] - oldY, 2);
			double currentPtCompare = pow(xPt[index] - currentX, 2) + pow(yPt[index] - currentY, 2);

			if ((compare < pow(baseRadius, 2)) && (currentPtCompare < pow(baseRadius, 2)))
			{
				//Ignore the point because the data is from the base itself
			}
			//The collisionCount is used to be able to make sure that there are multiple points there, not just one random point
			else if ((compare > pow(baseRadius, 2)) && (currentPtCompare < pow(baseRadius, 2)))
			{
				collisionCount++;
			}

			//If index == 0, then it is at the first point, so no modification is needed to be made
			if ((i == 0) && (collisionCount == 5))
			{
				moveToPt[0] = currentX;
				return moveToPt;
				break;
			}
			//This goes back to the index before the tested index to move the user to that position before hitting the obstacle
			else if ((collisionCount == 5) && (i != 0))
			{
				currentX = currentX - tempX;
				moveToPt[0] = currentX;
				return moveToPt;
				break;
			}
		}
		currentX = currentX + tempX;
	}
	moveToPt[0] = newX;
	return moveToPt;
}

/*Receives the command from the user.  Using this command, it will apply the transformation to a ghost frame to go check the sent position with the lidar data.
* It will find the best position to get to along the path without any collisions and will send these values to the Mobility Base.
* On top of that, it uses the roboFrame and sends the position to the GUI to track the positioning of the Mobility Base.*/
void receiveCommand(BoostSoc* guiSocket, BoostSoc* baseClient , boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, std::string centerShape)
{
	if (guiSocket->getConnected() && baseClient->getConnected())
	{
		//If the message isn't a blank message, update the text with that message
		//Splits up the incoming messages
		//For each message, split it up into its individual components (x, y, angle, speed)
		std::string incomingMessage = guiSocket->getMessage();

		if (incomingMessage != "")
		{
			std::vector<std::string> messages = split(incomingMessage.c_str(), ';');

			if (messages.size() <= 4)
			{
				for (int i = 0; i < messages.size() - 1; i++)
				{
					std::vector<std::string> recentValues = split(messages.at(i).c_str(), ',');
					double inputX = stod(recentValues[0]);
					double inputY = stod(recentValues[1]);
					double inputAngle = stod(recentValues[2]) * M_PI/180;	
					double speed = stod(recentValues[3]);

					double xPos = roboFrame(0, 3);
					double yPos = roboFrame(1, 3);

					//Applies the transformation to the ghost frame to check the positioning to see if there are any interferences 
					translate(ghostFrame, inputX, inputY);
					
					/*Sends to function to see if there are any lidar points in the way of the given command.
					* If there is, then check would equal to 0 and therefore fail, not allowing the mobility base to move to the specified point
					* If there aren't any collision points, then it would transform the roboFrame and send the movement command to the Mobility Base*/
					double *check = comparePoints(ghostFrame, visFrame);

					//Revert back to starting position for the ghost frame to check the next command
					translate(ghostFrame, -inputX, -inputY);

					//Sets the new input values after checking to see if there were any collision points
					inputX = check[0];
					inputY = check[1];

					//Takes the points of the function and stores them into a message (baseCmd) and sends it to the Mobility Base to control it
					std::string baseCmd = std::to_string(inputX);
					baseCmd += ", ";
					baseCmd += std::to_string(inputY);
					baseCmd += ", ";
					baseCmd += std::to_string((inputAngle*(180/M_PI)));
					baseCmd += ", ";
					baseCmd += std::to_string(speed);
					baseCmd += ";";

					translate(roboFrame, inputX, inputY);
					rotateAny(roboFrame, Eigen::Vector3f(roboFrame(0, 3), roboFrame(1, 3), roboFrame(2, 3)), Eigen::Vector3f(roboFrame(0, 2), roboFrame(1, 2), roboFrame(2, 2)), inputAngle);

					//This rotates the cube to match up with the rotation on the Mobility Base 
					rotateAny(visFrame, Eigen::Vector3f(visFrame(0, 3), visFrame(1, 3), visFrame(2, 3)), Eigen::Vector3f(visFrame(0, 2), visFrame(1, 2), visFrame(2, 2)), inputAngle);
					viewer->updateShapePose(centerShape, Eigen::Affine3f(visFrame));

					//Sends the updated command to the base to run
					std::cout << "Command Sent!" << std::endl;
					baseClient->sendMessage(baseCmd); 

					//Sends the position of the Mobility Base to the GUI to display to the user
					xPos = roboFrame(0, 3);
					yPos = roboFrame(1, 3);

					std::string positionUpdate = std::to_string(xPos);
					positionUpdate += ", ";
					positionUpdate += std::to_string(yPos);
					positionUpdate += ";";
					guiSocket->sendMessage(positionUpdate);
				}
			}
		}

	}
	else if (guiSocket->getConnected())
	{
		std::cout << "Waiting on Base Connection..." << std::endl;
	}
	else if (baseClient->getConnected()) 
	{
		std::cout << "Waiting on GUI Connection..." << std::endl;
	}
	else
	{
		std::cout << "Listening..." << std::endl;
	}
}